---
title: "Rite de Passage"
description: "PNJ du Rite de Passage"
date: 2024-05-02T23:46:11+02:00
weight: 3
featured_image: "01_darestan.jpg"
featured: false
resources:
- src: "01_darestan.jpg"
  title: Darestan, fils de Varankos de .
- src: "02_esrala.jpg"
  title: Esrala, fille de Kulvil, Initiée d'Ernalda.
- src: "03_aventarl.png"
  title: Aventarl, fils de Rosonil d'Ours Déchu.
- src: "04_ofin.png"
  title: Ofin, fils de Robasart, fils d'un fermier non-libre.
- src: "05_hengall.jpg"
  title: Hengall Coeur-étoilé, fils de Vingkot.
- src: "06_broo1.png"
  title: Broo tête de chèvre.
- src: "07_broo2.jpg"
  title: Broo tête de chèvre hurlante.
- src: "08_broo3.jpg"
  title: Broo tête d'élan.
- src: "09_broo4.png"
  title: Broo tête de cerf.
---
