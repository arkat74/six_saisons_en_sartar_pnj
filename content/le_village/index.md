---
title: "Le Village"
description: "Liste des PNJ du village"
date: 2024-05-02T23:46:11+02:00
weight: 2
featured_image: "gordangar.jpg"
featured: false
resources:
- src: "01_gordangar.jpg"
  title: Gordangar, fils de Kenstrel de Deux-fois Bénis, Chef et Seigneur des Vents.
- src: "02_savan.jpg"
  title: Savan, fils de Kenstrel de Deux-fois Bénis grand prêtre et Voix des Tempêtes.
- src: "03_jorgunath.jpg"
  title: Jorgunath Lame-chantante, anciennement de Deux-fois Bénis, chef Thane d'armes et Épée d'Humakt.
- src: "04_morganeth.jpg"
  title: Morganeth, fille de Jarlarant de Hautes-eaux, Grande Prêtresse d'Ernalda.
- src: "05_jodi.jpg"
  title: Jodi Élaphe-blanc de Guet du Passage Sage de Lhankor Mhy.
- src: "06_erinina.jpg"
  title: Erinina Hache-de-cuivre d'Ours Déchu, Seigneur runique de Babeester Gor.
- src: "07_keladon.jpg"
  title: Keladon Oeil-bleu, Trompeur Eurmali lié, grand Scalde.
- src: "08_borkar.jpg"
  title: Borkar, fils de Gudinn du Guet du Passage, Prince Marchand de Languedorée.
- src: "09_harvarr.jpg"
  title: Harvarr, fils d'Horvik de Bouclier de Falaise, Maître Forgeron.
- src: "10_jorna.jpg"
  title: Jorna Voix-chantante, femme de Gordangar, initiée d'Ernalda.
- src: "11_korra.jpg"
  title: Korra Long-doigt, femme de Savan, initiée de Ty Kora Tek.
- src: "12_le_cerf_royal.jpg"
  title: Le Cerf royal, fils du Cerf noir, chef des Bêtes du Val, shaman de l'Élaphe blanc.
---
