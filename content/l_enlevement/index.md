---
title: "L'enlèvement"
date: 2024-05-02T23:46:11+02:00
weight: 5
featured_image: "01_kordara.jpg"
featured: false
resources:
- src: "01_kordara.jpg"
  title: Kordara, fille d'Ara, "Grand-mère d'Écorce Blanche", initiée d'Ernalda.
- src: "02_fiedor.jpg"
  title: Fiedor, fils de Faffir du hameau d'Écorce Blanche.
- src: "03_soratha.jpg"
  title: Soratha, fille d'Umandor.
- src: "04_daladra.jpg"
  title: Daladra, fille de Thargan, femme de Karenstar.
- src: "05_karenstar.jpg"
  title: Karenstar, fils de Groll, thane d'armes.
---
