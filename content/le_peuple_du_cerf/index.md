---
title: "Le Peuple du Cerf"
date: 2024-05-02T23:46:11+02:00
weight: 6
featured_image: "01_gorvara.png"
featured: false
resources:
- src: "01_gorvara.png"
  title: Gorvara Cape-rouge commandant du Troisième vent, initiée de Vinga.
- src: "02_korolmath.jpg"
  title: Korolmath Briseur-de-bouclier commandant en second du Troisième vent, initié d'Orlanth Aventureux.
- src: "03_yolanva.jpg"
  title: Yolanva Saedrius de Dunstop, envoyée lunar.
- src: "04_andrin_fils_kul.jpg"
  title: Andrin Fils de Kul de Vinclair, représentant du roi de la tribu Colymar.
- src: "05_ordrinn.jpg"
  title: Ordrinn Fils de Morinn, Pisteur Sambari, initié d'Odayla.
- src: "06_sculture_dragon.jpg"
  title: Sculture de dragon dans le temple.
- src: "07_shah_vashak.jpg"
  title: Shah'vashak, la Wyrm arc-en-ciel.
---
