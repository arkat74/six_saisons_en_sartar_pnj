---
title: "Déguisé en mouton"
date: 2024-05-02T23:46:11+02:00
weight: 4
featured_image: "01_drugalla.jpg"
featured: false
resources:
- src: "01_drugalla.jpg"
  title: Drugalla Joufflue, épouse de Borkar.
- src: "02_andrin_fils_gurhas.png"
  title: Andrin, fils de Gurhas de Bouclier de Falaise.
- src: "03_keogor.png"
  title: Keogor, fils de Tarn de Coude de la Rivière.
- src: "04_ashart.jpg"
  title: Ashart, fils de Beroth fermier non-libre.
- src: "05_beroth.jpg"
  title: Beroth, fils de Beyor, fermier non-libre.
- src: "06_keoara.png"
  title: Keoara, fille de Tarn Yeux-perçant, jumelle en deuil.
---
